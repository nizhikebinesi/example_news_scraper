import scrapy
from scrapy.loader import ItemLoader
from scrapy.http import HtmlResponse
from news_scraper.items import NewsScraperItem


class BahbonnSpider(scrapy.Spider):
    name = 'bahbonn'
    allowed_domains = ['bah-bonn.de']

    def __init__(self, start_urls):
        super().__init__()
        self.start_urls = start_urls

    def parse(self, response: HtmlResponse):
        items = response.xpath(
            '//ul[contains(@class, "list") and contains(@class, "press")]/li'
        )
        for item in items:
            date = item.xpath('.//span[contains(@class, "date")]/text()').get()
            link = item.xpath('.//a/@href').get()
            title = item.xpath(".//a/text() | .//a/descendant::*/text()").getall()
            description = item.xpath('.//p/span[contains(@class, "date")]/following-sibling::text()[1]').get()

            yield response.follow(
                link,
                self.parse_release_page,
                cb_kwargs={
                    "publication_date": date,
                    "title": title,
                    "description": description,
                }
            )

    def parse_release_page(self, response: HtmlResponse, publication_date: str, title: str, description: str):
        loader = ItemLoader(item=NewsScraperItem(), response=response)
        loader.add_value("title", title)
        loader.add_value("description", description)
        loader.add_value("url_source", response.url)
        loader.add_value("publication_date", publication_date)
        loader.add_xpath("language", "//html/@lang")
        loader.add_xpath("body", '//div[contains(@class, "bodytext")]/text() '
                                 '| //div[contains(@class, "bodytext")]/descendant::*/text()')

        references = response.xpath('//div[contains(@class, "bodytext")]//a')
        loader.add_value("references", references)

        yield loader.load_item()
