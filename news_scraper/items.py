# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose, TakeFirst, Join, Compose


def handle_reference(a):
    tmp = {}
    tmp["url"] = a.xpath("./@href").get()
    title = a.xpath("./descendant::*/text()").get()
    if title:
        tmp["title"] = title.strip()
    return tmp


def prepare_texts(text):
    text = text.strip()
    return text


def prepare_description(description):
    description = description.lstrip("–").lstrip("-").strip()
    return description


def custom_join(texts):
    new_texts = []
    for t in texts:
        tmp = t.strip()
        if len(tmp) > 0:
            new_texts.append(tmp)
    return "\n".join(new_texts)


class NewsScraperItem(scrapy.Item):
    url_source = scrapy.Field(output_processor=TakeFirst())
    title = scrapy.Field(
        input_processor=MapCompose(prepare_texts),
        output_processor=Compose(custom_join)
    )
    authors = scrapy.Field()
    body = scrapy.Field(
        input_processor=MapCompose(prepare_texts),
        output_processor=Compose(custom_join)
    )
    description = scrapy.Field(
        input_processor=MapCompose(prepare_description),
        output_processor=Compose(custom_join)
    )
    publication_date = scrapy.Field(output_processor=TakeFirst())
    tags = scrapy.Field()
    references = scrapy.Field(input_processor=MapCompose(handle_reference))
    language = scrapy.Field(output_processor=TakeFirst())
