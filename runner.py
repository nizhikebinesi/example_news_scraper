from news_scraper import settings
from scrapy.settings import Settings
from scrapy.crawler import CrawlerProcess
from news_scraper.spiders.bahbonn import BahbonnSpider


if __name__ == "__main__":
    crawler_settings = Settings()
    crawler_settings.setmodule(settings)

    start_urls = [
        "https://www.bah-bonn.de/presse/pressemitteilungen/",
        "https://www.bah-bonn.de/presse/pressemitteilungen/pressearchiv/"
    ]
    process = CrawlerProcess(settings=crawler_settings)
    process.crawl(BahbonnSpider, start_urls=start_urls)

    process.start()
